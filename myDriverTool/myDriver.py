# -*- coding: utf-8 -*-
# @project : testOPMS
# @author  : lenovo
# @file     : myDriver.py
# @ide     : PyCharm
# @time    : 2020/12/25 20:07
from utils.mySettings import driverPtah, url
from selenium import webdriver


class Driver:
    """浏览器工具类"""
    # 初始化一个为 None 的driver
    _driver = None

    @classmethod
    def get_driver(cls, broser_name="Chrome"):
        """
        获取浏览器驱动对象
        :param broser_name: 控制浏览器的类型
        :return:
        """
        # 判断，是否为空
        if cls._driver is None:
            if broser_name == "Chrome":
                cls._driver = webdriver.Chrome()
            elif broser_name == "Firefox":
                cls._driver = webdriver.Firefox(driverPtah[broser_name])

            # 最大化窗口
            cls._driver.maximize_window()
            # 访问默认的网页
            cls._driver.get(url)
            # 执行登录
            cls.__login()

        return cls._driver

    @classmethod
    def __login(cls):
        """
        私有方法， 只能在类里边使用
        类外部无法使用，子类也不能继承
        只在浏览器刚打开的时候，执行一次登录
        :return:
        """
        cls._driver.find_element_by_name("username").send_keys("libai")
        cls._driver.find_element_by_name("password").send_keys("opmsopms123")
        cls._driver.find_element_by_css_selector("button").click()

if __name__ == '__main__':
    Driver.get_driver()
