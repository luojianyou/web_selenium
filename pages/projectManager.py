# -*- coding: utf-8 -*-
# @project : testOPMS
# @author  : lenovo
# @file     : projectManager.py
# @ide     : PyCharm
# @time    : 2020/12/25 20:27
"""项目管理页面，页面类"""
from utils.mySettings import pages_url_dir
from selenium.webdriver.common.by import By
from pages.basePage import BasePage
import time


class Projectmanager(BasePage):

    def to_page(self):
        """
        访问此页面的网址
        不必要预先定义页面的打开顺序，因为这样会让整个业务逻辑变得过于复杂
        考虑到我们手工测试的时候，就是执行到哪一个步骤，需要访问网址
        就去输入网址并回车访问
        所以，我们在每个页面类定义一个访问当前页面的函数
        需要用到的时候，就调用
        这个逻辑比写在初始化方法里边更简洁，更容易实现和维护
        :return:
        """
        # 经过实际运行之后，我们发现这里会有运行失败的情况
        # 不是代码错误，而是浏览器跟不上
        # 所以用sleep
        time.sleep(3)
        self.driver.get(pages_url_dir["Projectmanager"])

    def project_status_select_box(self):
        """项目状态搜索下拉框"""
        return self.get_element((By.NAME, "status"))

    def project_name_input_box(self):
        """项目名称搜索输入框"""
        return self.get_element((By.CSS_SELECTOR, "form > input"))

    def search_button_box(self):
        """搜索按钮"""
        return self.get_element((By.CSS_SELECTOR, "button[class=\"btn btn-primary\"]"))

    def create_project_button_box(self):
        """新建项目按钮"""
        return self.get_element((By.CSS_SELECTOR, "a[class=\"btn btn-success\"]"))

    def list_of_project_name_box(self):
        """匹配每一个项目名称，返回元素列表"""
        return self.get_elements((By.CSS_SELECTOR, "tbody > tr > :nth-child(1)"))

    def list_of_project_ali_name(self):
        """匹配每一个项目别名，返回元素列表"""
        return self.get_elements((By.CSS_SELECTOR, "tbody > tr > :nth-child(2)"))


class ProjectmanagerAction(Projectmanager):
    pass


PA = ProjectmanagerAction()

if __name__ == '__main__':
    PA.to_page()
    PA.project_name_input_box().send_keys("123")
