# -*- coding: utf-8 -*-
import os
import sys
import time
import logging

# log_path是存放日志的路径
log_path = os.path.dirname(os.getcwd()) + '/logs/'
print("log保存路径:" + log_path)
# 如果不存在这个logs文件夹，就创建一个
if not os.path.exists(log_path):
    os.mkdir(log_path)
sys.stdout.flush()  # 刷新缓冲区


class MyLogger(object):

    def __init__(self, class_name):
        # 创建一个logger
        self.logger = logging.getLogger(class_name)
        self.logger.setLevel('DEBUG')

        # 文件的命名
        self.log_name = os.path.join(log_path, '%s.log' % time.strftime('%Y_%m_%d_%H_%M_%S'))
        formatter = logging.Formatter("[%(asctime)s] [%(filename)s -%(name)s().%(funcName)s：%(lineno)d] -%(levelname)s："
                                      "%(message)s")
        # 创建一个FileHandler，用于写到本地
        fh = logging.FileHandler(self.log_name, 'a', encoding='utf-8')  # 这个是python3的
        fh.setLevel('DEBUG')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        '''
        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel('DEBUG')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)  # 给logger添加handler
        ch.close()
        '''
    def get_logger(self):
        return self.logger