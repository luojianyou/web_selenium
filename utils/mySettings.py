# -*- coding: utf-8 -*-
# @project : testOPMS
# @author  : lenovo
# @file     : mySettings.py
# @ide     : PyCharm
# @time    : 2020/12/25 20:04

# 网址
url = "http://127.0.0.1:8088"
# 超时时间
time_out = 10
# 轮询时间
poll_time = 0.5
# driver 路径
driverPtah = {
    "Chrome":"D:\\tool\selenium\chromedriver.exe"
}

pages_url_dir = {
    "Projectmanager": "http://127.0.0.1:8088/project/manage"
}
report_path = '..\\report'