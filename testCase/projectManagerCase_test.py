# -*- coding: utf-8 -*-
# @project : testOPMS
# @author  : lenovo
# @file     : projectManagerCase.py
# @ide     : PyCharm
# @time    : 2020/12/25 21:05
from pages.projectManager import PA
import shutil
import pytest
import sys
import os
import allure
from logs.mylog import MyLogger
from utils.mySettings import report_path
from common.getImage import SaveImage
from pages.projectManager import Projectmanager
log = MyLogger("testOPMS").get_logger()
class TestProjectManagerCase():

    def test_project_name_search(self):
        """
        模糊查询测试用例，当我通过某字符串搜索项目的时候
        查询出来的每一条信息，它的名称或者别名
        至少有一个包含我给定的字符串
        :return:
        """

        PA.to_page()

        """1 选定文本，输入并搜索"""
        project_name = "06-25"
        PA.project_name_input_box().send_keys(project_name)
        PA.search_button_box().click()
        log.info(f"输入日期{project_name}成功")
        SaveImage(PA.driver,'project_name_input_sucess.png')
        """2 获取项目名称列表， 获取项目别名列表"""
        project_name_sli = PA.list_of_project_name_box()
        project_ali_name_sli = PA.list_of_project_ali_name()

        """3 验证断言，搜索出来的列表，别名或项目名称中至少有一个包含我搜索的文本"""
        for project_name_for in project_name_sli:
            as1 = project_name in project_name_for.text
            as2 = project_name in project_ali_name_sli[project_name_sli.index(project_name_for)].text

            assert (as1 or as2)


if __name__ == '__main__':
    shutil.rmtree(report_path) #删除report文件夹1
    pytest.main(['projectManagerCase_test.py','-s','--alluredir','../report'])  #-s表示执行print语句
    os.system('allure generate ../report -o ../report/report --clean')  #生成allure报告并清除之前的内容

